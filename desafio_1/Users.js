// Challenge 1: Create a class that models an user who can hae pets abd books.
class Users {
    constructor(name, lastname) {
        this.name = name;
        this.lastname = lastname;
        this.pets = [];
        this.books = [];
          
    }
    getFullName() {
        return `El nombre de este usuario es: ${this.name} ${this.lastname}`;
    }

    addPet(pet) {  // agrega una mascota
        this.pets.push(pet);
    }
    addBook(book) {    //Añade un libro a la lista de libros
        this.books.push(book);
    }
    countPets() { //Cuenta las mascotas
        return this.pets.length;
    }
    getBooksName() { //Devuelve el nombre de los libros
        this.books.forEach(book => {
            console.log(book);
            } 
        );
    }

}

//Creates a new user
const user1 = new Users('Juan', 'Perez');

//Adds a pet to the user
user1.addPet('Perro');
user1.addPet('Gato');
//Adds a book to the user
user1.addBook({name: 'Harry Potter', author: 'J.K. Rowling'});

user1.addBook('El Hobbit');

//Show it on conse
console.log(user1.getFullName());
console.log(user1.countPets());
console.log(user1.books);